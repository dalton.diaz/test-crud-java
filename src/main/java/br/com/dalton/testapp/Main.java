package br.com.dalton.testapp;
import javax.persistence.EntityManager;

import br.com.dalton.testapp.connection.PersistenceManager;
import br.com.dalton.testapp.user.User;
 
public class Main {
  public static void main(String[] args) {
    User u = new User();
    u.setName("Dalton");
    u.setLastname("D");
    u.setPassword("123");
    u.setEmail("dalton@gitlab.com");
    
    EntityManager em = PersistenceManager.INSTANCE.getEntityManager();
    em.getTransaction()
        .begin();
    em.persist(u);
    em.getTransaction()
        .commit();
 
    em.close();
    PersistenceManager.INSTANCE.close();
  }
}